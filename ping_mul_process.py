# Nur um es zu verstehen am Beispiel

import subprocess
from multiprocessing import Process

class PingPong:
    ''' ping wird in einem subprocess ausgeführt
    ACHTUNG hier werden zum Test die Parameter auf einem Linux system gesetzt '''
    def __init__(self, ping_address="8.8.8.8"):
        self.ping_address = ping_address

    def start_ping(self):
        ''' ping mit der IP der Klassen Instanz '''
        data = subprocess.run(['ping', self.ping_address, '-c4'], capture_output=True)
        print(data)
    
    def ping(self, ip):
        ''' ping der ip nicht aus der Klassen Instanz '''
        data = subprocess.run(['ping', ip, '-c4'], capture_output=True)
        print(data)        

def loping(self, ip):
    ''' ping auserhalb der Klasse '''
    data = subprocess.run(['ping', ip, '-c4'], capture_output=True)
    print(data)    

if __name__ == "__main__":
    # einen Pool von IP Adressen
    # keine Limitierung der Anzahl der Prozesse
    ip_adressen = ['127.0.0.1', '8.8.8.8', '1.1.1.1', '192.58.128.30']
    ping_classen = []
    for ip in range(len(ip_adressen)):
        ping = PingPong(ip_adressen[ip])
        ping_classen.append(Process(target=ping.start_ping))
        ping_classen[-1].start()

    for pings in ping_classen:
        pings.join()